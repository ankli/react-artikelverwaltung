import React, { useState } from 'react';
import './App.css';
import Display from '../components/Display';
import KeyPad from '../components/KeyPad';

    const App: React.FC = () => {
      const [currentValue, setCurrentValue] = useState<string>('');


      const handleKeyPress =(keyValue: string) => {
        setCurrentValue(currentValue + keyValue);
      }

      const handleCalculate = () => {
        setCurrentValue(eval(currentValue));
      }

      return (
          <div className="App">
              <Display value={currentValue} />
              <KeyPad onKeyPress={handleKeyPress} onCalculate={handleCalculate} />
          </div>
      )}

export default App;