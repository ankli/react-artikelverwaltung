import React from 'react';

interface Props {
    value?: String;
}

const Display: React.FC<Props> = (props) => {
    const currentValue = props.value ? props.value : '-';
    return <div>{currentValue}</div>
}

export default Display;