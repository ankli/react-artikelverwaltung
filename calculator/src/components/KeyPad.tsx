import React from 'react';

interface Props {
    onKeyPress: (keyValue: string) => void;
    onCalculate: () => void;
}

const KeyPad: React.FC<Props> = (props) => {
   const generateKeyPadButton = (keyValue: string) => {
       return (
        <button onClick={() => props.onKeyPress(keyValue)}>{keyValue}</button>
       )
   }    
   

    return <div>
        {generateKeyPadButton('1')}
        {generateKeyPadButton('2')}
        {generateKeyPadButton('3')}        
        <br/>
        {generateKeyPadButton('4')}
        {generateKeyPadButton('5')}
        {generateKeyPadButton('6')}
        <br/>
        {generateKeyPadButton('7')}
        {generateKeyPadButton('8')}
        {generateKeyPadButton('9')}
        <br/>
        {generateKeyPadButton('0')}
        {generateKeyPadButton('+')}
        {generateKeyPadButton('-')}
        <br/>
        <button onClick={props.onCalculate}>=</button>
    </div>;
}

export default KeyPad;