import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const [value, setValue] = useState(0);

  const onMinusClicked = () =>{
    setValue(value - 1);
  }

  const onPlusClicked = () =>{
    setValue(value + 1);
  }

  const mystyle = {
    color: '#FF0000'
  }

  return (
    <div style={mystyle}>
      <button onClick={onMinusClicked}>-1</button>
      <span>{value}</span>
      <button onClick={onPlusClicked}>+1</button>
    </div>
  );
}

export default App;
